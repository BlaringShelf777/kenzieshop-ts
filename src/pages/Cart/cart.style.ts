import styled from "styled-components"

export const Container = styled.div`
	padding: 2rem;
	display: flex;
	flex-wrap: wrap;
	gap: 1rem;
	justify-content: center;
	align-items: center;
`

export const OrderSummary = styled.div`
	flex: 100%;
	padding: 2rem;
	border: 1px solid skyblue;
	border-radius: 1rem;

	p {
		font-size: 1.5rem;
		margin-bottom: 1rem;
	}
`
