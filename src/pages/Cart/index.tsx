import Product from "../../components/Product"
import { useUser } from "../../Providers/User"
import { Container, OrderSummary } from "./cart.style"

const formatValue = (value: number) =>
	Intl.NumberFormat("pt-BR", {
		style: "currency",
		currency: "BRL",
	}).format(value)

const Cart = () => {
	const { cart } = useUser()

	return (
		<Container>
			<OrderSummary>
				<p>Order Summary</p>
				{formatValue(cart.reduce((acc, cur) => acc + cur.price, 0))}
			</OrderSummary>
			{cart.map((product) => (
				<Product key={product.id} product={product} isInCart />
			))}
		</Container>
	)
}

export default Cart
