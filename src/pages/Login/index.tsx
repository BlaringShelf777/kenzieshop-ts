import { TextField, Button } from "@material-ui/core"
import { useForm } from "react-hook-form"
import { yupResolver } from "@hookform/resolvers/yup"
import * as yup from "yup"

import { Container } from "./login.style"
import { useUser } from "../../Providers/User"
import { useHistory } from "react-router-dom"

interface IData {
	email: string
	password: string
}

const Login = () => {
	const schema = yup.object().shape({
		email: yup.string().required().email(),
		password: yup.string().min(6, "Mínimo de 6 dígitos").required(),
	})

	const { login } = useUser()

	const history = useHistory()

	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm({
		resolver: yupResolver(schema),
	})

	const onSubmit = async (data: IData) => {
		if (await login(data)) {
			history.push("/")
		}
	}

	return (
		<Container>
			<form onSubmit={handleSubmit(onSubmit)}>
				<div>
					<TextField
						{...register("email")}
						margin="normal"
						variant="outlined"
						label="Email"
						name="email"
						size="small"
						color="primary"
						error={!!errors.email}
						helperText={errors.email?.message}
					/>
				</div>

				<div>
					<TextField
						{...register("password")}
						margin="normal"
						variant="outlined"
						label="Password"
						name="password"
						size="small"
						color="primary"
						type="password"
						error={!!errors.password}
						helperText={errors.password?.message}
					/>
				</div>
				<Button type="submit" variant="contained" color="primary" size="small">
					send
				</Button>
			</form>
		</Container>
	)
}

export default Login
