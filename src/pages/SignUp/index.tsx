import { TextField, Button } from "@material-ui/core"
import { useForm } from "react-hook-form"
import { yupResolver } from "@hookform/resolvers/yup"
import * as yup from "yup"
import { Container } from "./signup.style"
import { useUser } from "../../Providers/User"

interface IData {
	username: string
	email: string
	password: string
	_password: string
}

const Signup = () => {
	const { register: signup } = useUser()

	const schema = yup.object().shape({
		username: yup.string().required(),
		email: yup.string().required().email(),
		password: yup.string().min(6, "Mínimo de 6 dígitos").required(),
		_password: yup
			.string()
			.oneOf([yup.ref("password")], "Passwords must match"),
	})

	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm({
		resolver: yupResolver(schema),
	})

	const onSubmit = ({ username, email, password }: IData) => {
		signup({ username, email, password })
	}

	return (
		<Container>
			<form onSubmit={handleSubmit(onSubmit)}>
				<div>
					<TextField
						{...register("username")}
						margin="normal"
						variant="outlined"
						label="Nome de usuário"
						name="username"
						size="small"
						color="primary"
						error={!!errors.username}
						helperText={errors.username?.message}
					></TextField>
				</div>

				<div>
					<TextField
						{...register("email")}
						margin="normal"
						variant="outlined"
						label="Email"
						name="email"
						size="small"
						color="primary"
						error={!!errors.email}
						helperText={errors.email?.message}
					/>
				</div>

				<div>
					<TextField
						{...register("password")}
						type="password"
						margin="normal"
						variant="outlined"
						label="Password"
						name="password"
						size="small"
						color="primary"
						error={!!errors.password}
						helperText={errors.password?.message}
					/>
				</div>

				<div>
					<TextField
						{...register("_password")}
						type="password"
						margin="normal"
						variant="outlined"
						label="Confirm password"
						name="_password"
						size="small"
						color="primary"
						error={!!errors._password}
						helperText={errors._password?.message}
					/>
				</div>

				<Button type="submit" variant="contained" color="primary" size="small">
					send
				</Button>
			</form>
		</Container>
	)
}

export default Signup
