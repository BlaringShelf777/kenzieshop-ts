import Product from "../../components/Product"
import { useProducts } from "../../Providers/Products"
import { Container } from "./home.style"

const Home = () => {
	const { products } = useProducts()

	return (
		<Container>
			{products.map((product) => (
				<Product key={product.id} product={product} />
			))}
		</Container>
	)
}

export default Home
