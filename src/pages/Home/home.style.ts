import styled from "styled-components"

export const Container = styled.div`
	padding: 2rem;
	display: flex;
	flex-wrap: wrap;
	gap: 1rem;
	justify-content: center;
	align-items: center;
`
