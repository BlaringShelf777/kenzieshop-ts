import { ReactNode } from "react"
import { useState } from "react"
import { useEffect } from "react"
import { useContext } from "react"
import { createContext } from "react"
import { toast } from "react-toastify"
import { api } from "../../services/api"

interface IUserData {
	cart: IProduct[]
	addToCart: (product: IProduct) => void
	removeFromCart: (product: IProduct) => void
	register: (data: IRegister) => Promise<boolean>
	login: (data: ILogin) => Promise<boolean>
	auth: string
}

interface IUserProviderProps {
	children: ReactNode
}

interface IProduct {
	name: string
	image_url: string
	price: number
	description: string
	id: number
}

interface ILogin {
	email: string
	password: string
}

interface IRegister {
	username: string
	password: string
	email: string
}

const UserContext = createContext<IUserData>({} as IUserData)

export const UserProvider = ({ children }: IUserProviderProps) => {
	const [cart, setCart] = useState<IProduct[]>([])
	const [auth, setAuth] = useState<string>("")

	const addToCart = (product: IProduct) => {
		if (!cart.find((p) => p.id === product.id)) {
			const new_cart = [...cart, product]
			setCart(new_cart)
			toast.success("Product added to cart!")
			localStorage.setItem("@KS:cart", JSON.stringify(new_cart))
		} else {
			toast.error("Product already in cart!")
		}
	}

	const removeFromCart = (product: IProduct) => {
		const new_cart = cart.filter((p) => p.id !== product.id)

		setCart(new_cart)
		localStorage.setItem("@KS:cart", JSON.stringify(new_cart))
	}

	const register = async (data: IRegister) => {
		let success = false

		await api
			.post("/register/", data)
			.then(({ data: { accessToken } }) => {
				setAuth(accessToken)
				localStorage.setItem("@KS:auth", accessToken)
				success = true
			})
			.catch(() => toast.error("Ops... Something went wrong!"))

		return success
	}

	const login = async (data: ILogin) => {
		let success = false

		await api
			.post("/login/", data)
			.then(({ data: { accessToken } }) => {
				setAuth(accessToken)
				localStorage.setItem("@KS:auth", accessToken)
				success = true
			})
			.catch(() => toast.error("Username or password incorrect!"))

		return success
	}

	useEffect(() => {
		const authorization = localStorage.getItem("@KS:auth")
		const myCart = localStorage.getItem("@KS:cart")

		if (!!authorization) {
			setAuth(JSON.parse(authorization))
		}
		if (!!myCart) {
			setCart(JSON.parse(myCart))
		}
	}, [])

	return (
		<UserContext.Provider
			value={{ register, login, auth, cart, addToCart, removeFromCart }}
		>
			{children}
		</UserContext.Provider>
	)
}

export const useUser = () => useContext(UserContext)
