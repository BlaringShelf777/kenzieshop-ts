import { useContext } from "react"
import { useEffect } from "react"
import { useState } from "react"
import { ReactNode } from "react"
import { createContext } from "react"
import { api } from "../../services/api"

interface IProduct {
	name: string
	image_url: string
	price: number
	description: string
	id: number
}

interface IProductsProps {
	children: ReactNode
}

interface IApiResponse {
	data: IProduct[]
}

interface IProductsData {
	products: IProduct[]
}

const ProductsContext = createContext<IProductsData>({} as IProductsData)

export const ProductsProvider = ({ children }: IProductsProps) => {
	const [products, setProducts] = useState<IProduct[]>([])

	useEffect(() => {
		api
			.get("/products")
			.then(({ data }: IApiResponse) => setProducts(data))
			.catch((err) => console.log(err))
	}, [])

	return (
		<ProductsContext.Provider value={{ products }}>
			{children}
		</ProductsContext.Provider>
	)
}

export const useProducts = () => useContext(ProductsContext)
