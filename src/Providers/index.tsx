import { ReactNode } from "react"
import { ProductsProvider } from "./Products"
import { UserProvider } from "./User"

interface IProviderProps {
	children: ReactNode
}

const Providers = ({ children }: IProviderProps) => {
	return (
		<ProductsProvider>
			<UserProvider>{children}</UserProvider>
		</ProductsProvider>
	)
}

export default Providers
