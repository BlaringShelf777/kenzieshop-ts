import { createGlobalStyle } from "styled-components"

export default createGlobalStyle`
    * {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    }

    :root {
        --gray: #2d2d2d;
    }

    a {
	    text-decoration: none;
    }

    button {
        cursor: pointer;
    }
`
