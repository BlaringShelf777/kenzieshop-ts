import React from "react"
import ReactDOM from "react-dom"
import { BrowserRouter as Router } from "react-router-dom"
import Header from "./components/Header"
import Providers from "./Providers"
import Routes from "./Routes"
import GlobalStyle from "./style/global"
import "react-toastify/dist/ReactToastify.css"
import { ToastContainer } from "react-toastify"

ReactDOM.render(
	<React.StrictMode>
		<Providers>
			<Router>
				<Header />
				<Routes />
				<GlobalStyle />
				<ToastContainer />
			</Router>
		</Providers>
	</React.StrictMode>,
	document.getElementById("root")
)
