import { Link } from "react-router-dom"
import styled from "styled-components"

export const Container = styled.div`
	padding: 1rem 2rem;
	background-color: skyblue;
	cursor: pointer;
	display: flex;
	justify-content: space-between;
	align-items: center;
`

export const Logo = styled.h1`
	font-size: 2rem;
	font-weight: bold;
	color: var(--gray);
`

export const NavMenu = styled.nav`
	display: flex;
	gap: 1rem;
`

export const NavItem = styled(Link)`
	color: var(--gray);
	position: relative;
`

export const ProductsCount = styled.div`
	position: absolute;
	top: -10px;
	right: -15px;
	z-index: 1;
	background-color: white;
	width: 20px;
	height: 20px;
	display: grid;
	place-items: center;
	border-radius: 50%;
	font-size: 0.8rem;
	font-weight: bold;
`
