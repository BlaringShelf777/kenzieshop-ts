import { Link } from "react-router-dom"
import { useUser } from "../../Providers/User"
import {
	Container,
	Logo,
	NavItem,
	NavMenu,
	ProductsCount,
} from "./header.style"

const Header = () => {
	const { cart } = useUser()
	const { auth } = useUser()

	return (
		<Container>
			<Logo as={Link} to="/">
				KenzieShop
			</Logo>
			<NavMenu>
				{!auth && (
					<>
						<NavItem to="/login">Login</NavItem>
						<NavItem to="/signup">Sign Up</NavItem>
					</>
				)}
				<NavItem to="/cart">
					Cart
					<ProductsCount>{cart.length}</ProductsCount>
				</NavItem>
			</NavMenu>
		</Container>
	)
}

export default Header
