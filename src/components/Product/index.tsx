import { useUser } from "../../Providers/User"
import { Container } from "./product.style"

interface IProduct {
	name: string
	image_url: string
	price: number
	description: string
	id: number
}

interface IProductProps {
	product: IProduct
	isInCart?: boolean
}

const formatValue = (value: number) =>
	Intl.NumberFormat("pt-BR", {
		style: "currency",
		currency: "BRL",
	}).format(value)

const Product = ({ product, isInCart = false }: IProductProps) => {
	const { name, description, image_url, price } = product
	const { addToCart, removeFromCart } = useUser()

	return (
		<Container>
			<img src={image_url} alt={name} />
			<h2>{name}</h2>
			<p>{description}</p>
			{!isInCart && <div>{formatValue(price)}</div>}
			<button
				onClick={
					isInCart ? () => removeFromCart(product) : () => addToCart(product)
				}
			>
				{isInCart ? "-" : "+"}
			</button>
		</Container>
	)
}

export default Product
