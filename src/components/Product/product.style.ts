import styled from "styled-components"

export const Container = styled.div`
	display: flex;
	flex-direction: column;
	padding: 1rem;
	gap: 0.5rem;
	width: 235px;
	height: 345px;
	border-radius: 1rem;
	border: 1px solid skyblue;
	position: relative;
	color: var(--gray);
	justify-content: space-between;

	img {
		height: 200px;
		width: fit-content;
		margin: 0 auto;
	}

	h2 {
		font-size: 1rem;
		max-width: 200px;
	}

	p {
		font-size: 0.8rem;
		max-width: 200px;
	}

	div {
		text-align: right;
		font-weight: bold;
	}

	button {
		position: absolute;
		top: 1rem;
		right: 1rem;
		width: 35px;
		height: 35px;
		border-radius: 50%;
		border: 0;
		background-color: skyblue;
		transition: opacity 0.3s ease;
		opacity: 0;
	}

	&:hover button {
		opacity: 1;
	}
`
