import { Switch } from "react-router-dom"
import Cart from "../pages/Cart"
import Home from "../pages/Home"
import Login from "../pages/Login"
import Signup from "../pages/SignUp"
import Route from "./Route"

const Routes = () => {
	return (
		<Switch>
			<Route component={Home} path="/" exact />
			<Route component={Cart} path="/cart" />
			<Route component={Login} path="/login" isPrivate />
			<Route component={Signup} path="/signup" isPrivate />
		</Switch>
	)
}

export default Routes
