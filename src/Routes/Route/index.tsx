import { Redirect, Route as DomRoute } from "react-router-dom"
import { useUser } from "../../Providers/User"

interface RouteProps {
	component: React.ComponentType
	isPrivate?: boolean
	path: string
	exact?: boolean
}

const Route = ({
	component: Component,
	isPrivate = false,
	path,
	exact = false,
}: RouteProps) => {
	const { auth } = useUser()

	return (
		<DomRoute
			path={path}
			exact={exact}
			render={() =>
				isPrivate && !auth ? (
					<Component />
				) : isPrivate && !!auth ? (
					<Redirect to="/" />
				) : (
					<Component />
				)
			}
		/>
	)
}

export default Route
